# Kubectl


### **Config:**
Damit kubectl meine Zugangsdaten benutzt, muss es wissen wo sich diese befinden.

Per Default befindet sich die Config-Datei unter **$HOME/.kube/config**

Sie kann aber auch per  Umgebungsvariable ***KUBECONFIG*** gesetzt werden. (Pfad zur Datei)

Oder aber als letzter Mittel kann der Datei-Pfad pro Aufruf der kubectl mitgegeben werden.
```
kubectl --kubeconfig=[PFAD]
```

---

### **PODs anzeigen:**
```
kubectl get pod
```

### **Services anzeigen:**
```
kubectl get service
```

### **Deployments anzeigen:**
```
kubectl get deployment
```

### **YAML-Dateien anwenden:**
```
kubectl apply -f [DATEI/PFAD]
```

### **Netzwerk Verbindung zu einem POD:**
```
kubectl port-forward [local-port]:[pod-port] [POD-NAME]
```

### **Genauere Infos zu PODs, Services, Deployments:**
```
kubectl describe [pod|service|deployment]
```
